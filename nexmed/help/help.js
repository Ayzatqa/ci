module.exports = {
    tags: ['Nexmed, doctor'],
    'Врач': function (client) {
        client
            .windowMaximize()
            .url('http://rc.nexmed.cargorun.ru/')
    },
    'Авторизация врача': function (client) {
        client
            .waitForElementVisible('input[name=Login]', 5000)
            .setValue('input[name=Login]', 'gajsin.rishat')
            .setValue('input[name=Password]', 'qwerty')
            .useXpath().click("//button[text()='Войти']")
            .pause(2000)
    },
    'Список пациентов': function (client) {
        client
            .useXpath().waitForElementVisible("//span[text()='Пациенты']", 5000)
            .assert.visible("//h4[text()='АРМ Врач']")
            .click("//span[text()='Пациенты']")
            .pause(2000)
            .waitForElementVisible("//td//a[5]", 5000)
            .click("//td//a[5]")
            .pause(20000)
            // .useXpath().waitForElementVisible("//td//a", 5000)
            // .click("//td//a")
    // },
    // 'Отклонение исследования': function (client) {
    //     client
    //         .pause(2000)
    //         .useCss().click('th[class="sorting_asc"]')
    //         .pause(1500)
    //         .keys(client.Keys.ARROW_DOWN)
    //         .keys(client.Keys.ARROW_DOWN)
    //         .keys(client.Keys.ARROW_DOWN)
    //         .pause(3000)
    //         //Кнопка Отменить
    //         .useCss().waitForElementVisible('button[data-title="Отмена назначения"]', 5000)
    //         .click('button[data-title="Отмена назначения"]')
    //         .pause(3000)
    //         //Кнопка Нет
    //         .assert.visible('button[class="cancel"]')
    //     //Кнопка Да
    //         .assert.visible('button[class="confirm"]')
    //         .click('button[class="confirm"]')
    //         .pause(3000)
            .end();
        client.perform(function (client) {
            console.log('Успешное завершение теста Врача');
            client()
        })
    }
};