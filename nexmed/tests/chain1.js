/**
 * Created by Mr. Ayzat on 07.10.2016.
 */
module.exports = {
    tags:['Nexmed, doctor'],
    'Врач': function (client) {
        client
            .windowMaximize()
            .url('http://rc.nexmed.ru/')
    },
    'Авторизация врача': function (client) {
        client
            .waitForElementVisible('input[name=Login]', 5000)
            .setValue('input[name=Login]', 'gajsin.rishat')
            .setValue('input[name=Password]', 'qwerty')
            .useXpath().click("//button[text()='Войти']")
    },
    'Список пациентов': function (client) {
        client
            .useXpath().waitForElementVisible("//span[text()='Пациенты']", 5000)
            .assert.visible("//h4[text()='АРМ Врач']")
            .click("//span[text()='Пациенты']")
    },
    'Добавление пациента': function (client) {
        client
            .useXpath().click("//a[text()='Добавить пациента']")
            .pause(2000)
    },
    'Заполнение общей информации': function (client) {
        client
            .useCss().setValue('input[name=SecondName]', 'Кашапов')
            .setValue('input[name=FirstName]', 'Роберт')
            .setValue('input[name=Patronymic]', 'Дмитриевич');
        client.setValue('input[name=Birthday]', ['20.11.1996', client.Keys.RETURN])
            .click('input[value=Female]')
            .setValue('input[name=Address]', 'г. Набережные Челны, дом 11, кв 5.')
    },
    'Заполнение личных данных': function (client) {
        client
            .useCss().setValue('input[name=Snils]', '152-125-124-12-29')
            .setValue('input[name=Polis]', '№3234364632')
            .setValue('input[name=PolisData]', 'Поликлиника №3')
            .setValue('input[name=Passport]', '9108 8522234')
            .setValue('input[name=PassportData]', 'ОУФМС по РФ, РБ, ленниа 25')
    },
    'Заполнение контактной информации': function (client) {
        client
            .setValue('input[name=PhoneNumber]', '+799933330011')
            .setValue('input[name=Email]', 'elvar@gmail.com')
    },
    'Добавить': function (client) {
        client
            .useXpath().click("//button[text()='Добавить']")
            .pause(4000);
    },
    'Пациент': function (client) {
        client
            .useXpath().waitForElementVisible("//a[text()='Кашапов Роберт Дмитриевич']", 5000)
            .click('//th[@class="sorting_asc"]')
            .pause(2000)
            .click("//a[text()='Кашапов Роберт Дмитриевич']")
            .pause(3000)
    },
    'Карточка пациента': function (client) {
        client
            //Кнопка Добавить исследование
            .useCss().waitForElementPresent('a[class="btn btn-primary btn-sm btn-xs pull-right"]', 8000)
            .click('a[class="btn btn-primary btn-sm btn-xs pull-right"]')
            .pause(3000)
    },
    'Добавление исследования': function (client) {
        client
            .useXpath().waitForElementVisible("//label[text()='Исследование']", 5000)
            .assert.visible("//label[text()='CITO']")
            //Поле Исследование
            .useCss().click('span[class="select2-selection select2-selection--single"]')
            .pause(2000)
            //Поле Поиска Исследования
            .setValue('input[type=search]', 'Мочевина')
            .pause(1500)
            .keys(client.Keys.ENTER)
            .click('ins[class=iCheck-helper]')
            .useXpath().click("//button[text()='Назначить']")
            .pause(2000)
            .useCss().waitForElementVisible('a[class="btn btn-primary btn-sm btn-xs pull-right"]', 5000)
            .click('a[class="btn btn-primary btn-sm btn-xs pull-right"]')
            .useXpath().waitForElementVisible("//label[text()='Исследование']", 5000)
            .assert.visible("//label[text()='CITO']")
            .useCss().click('span[class="select2-selection select2-selection--single"]')
            .pause(2000)
            .setValue('input[type=search]', 'Аланинаминотрансфераза')
            .pause(1500)
            .keys(client.Keys.ENTER)
            .click('ins[class=iCheck-helper]')
            .useXpath().click("//button[text()='Назначить']")
            .pause(2000)
            .useCss().waitForElementVisible('a[class="btn btn-primary btn-sm btn-xs pull-right"]', 5000)
            .click('a[class="btn btn-primary btn-sm btn-xs pull-right"]')
            .useXpath().waitForElementVisible("//label[text()='Исследование']", 5000)
            .assert.visible("//label[text()='CITO']")
            .useCss().click('span[class="select2-selection select2-selection--single"]')
            .pause(2000)
            .setValue('input[type=search]', 'Общий анализ крови')
            .pause(1500)
            .keys(client.Keys.ENTER)
            .click('ins[class=iCheck-helper]')
            .useXpath().click("//button[text()='Назначить']")
    },
    'Отклонение исследования': function (client) {
        client
            .pause(2000)
            .useXpath().click('//th[@class="sorting"][4]')
            .pause(2000);
            //Кнопка Отменить
        client.saveScreenshot('/nexmed/screens/vrach/otmenanazn.png')
            .useCss().waitForElementVisible('button[data-title="Отмена назначения"]', 5000)
            .click('button[data-title="Отмена назначения"]')
            .pause(3000)
            //Кнопка Нет
            .assert.visible('button[class="cancel"]')
            //Кнопка Да
            .assert.visible('button[class="confirm"]');
        client.saveScreenshot('/nexmed/screens/vrach/knopkaDa.png')
            .click('button[class="confirm"]')
            .pause(3000)
            .end();
        client.perform(function (client) {
            console.log('Успешное завершение теста Врача');
            client()
        })
    }
};