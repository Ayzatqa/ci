/**
 * Created by Mr. Ayzat on 07.10.2016.
 */
module.exports = {
    'Медсестра сортировочного кабинета больницы': function (client) {
        client
            .windowMaximize()
            .url('http://rc.nexmed.ru/')
    },
    'Авторизация медсестры сортировочного кабинета больницы': function (client) {
        client
            .waitForElementVisible('input[name=Login]', 2500)
            .setValue('input[name=Login]', "aminova.milyausha")
            .setValue('input[name=Password]', 'qwerty')
            .useXpath().click("//button[text()='Войти']")
    },
    'Входящиеe': function (client) {
        client
            .useXpath().waitForElementVisible("//span[text()='Входящие']", 5000)
            .click("//span[text()='Входящие']")
            .pause(3000)
    },
    'Проверка фильтрации по исследованию': function (client) {
        client
            .useCss().setValue('input[name="StudyFilter"]', 'крови')
            //Кнопка Фильтровать
            .click('button[class="btn btn-primary2"]')
            .pause(3000)
            .useXpath().assert.visible("//td")
            .click("//td")
    },
    'Проверка фильтрации по дате назначения': function (client) {
        client
            .useCss().clearValue('input[name="Date"]')
            .setValue('input[name="Date"]', '25.01.19')
            //Кнопка Фильтровать
            .click('button[class="btn btn-primary2"]')
            .useXpath().waitForElementVisible("//td[text()='В таблице отсутствуют данные']", 3000)
            .click("//span[text()='Входящие']")
            .waitForElementVisible("//td", 5000)
            .click("//td")
            .pause(3000)
            .pause(3000)
    },
    'Проверка отклонения исследований': function (client) {
        client
            //Кнопка Отклонить в таблице входящих исследований
            .useCss().waitForElementVisible('i[class="fa fa-close"]', 5000)
            .click('i[class="fa fa-close"]')
            //Причина отказа -> Другое
            .waitForElementVisible('select[class="form-control"]', 5000)
            .pause(1500)
            .useXpath().assert.visible("//h4[text()='Отклонение заявки']")
            .useCss().click('select[class="form-control"]')
            .pause(1500)
            .keys(client.Keys.ARROW_DOWN+client.Keys.RETURN)
            .pause(1500)
            //Кнопка Отклонить
            .click('button[class="btn btn-danger"]')
            .useXpath().waitForElementVisible("//button[text()='Продолжить']", 5000)
            .click("//button[text()='Продолжить']")
            .pause(1500)
            //Кнопка Отклонить в таблице входящих исследований
            .useCss().click('i[class="fa fa-close"]')
            .pause(1500)
            .useXpath().assert.visible("//h4[text()='Отклонение заявки']")
            //Причина отказа -> Другое
            .useCss().click('select[class="form-control"]')
            .keys(client.Keys.ARROW_DOWN+client.Keys.RETURN)
            .pause(1500)
            //Комментарий
            .setValue('textarea[name="CancelComment"]', 'Тестовое отклонение')
            //Кнопка Отклонить
            .click('button[class="btn btn-danger"]')
            .pause(2000)
    },
    'Вкладка Отправка': function (client) {
        client
            .useXpath().waitForElementVisible("//span[text()='Отправка']", 5000)
            .click("//span[text()='Отправка']")
            .pause(3000)
    },
    'Выбор исполнителя': function (client) {
        client
            //Исполнитель
            .useCss().waitForElementVisible('select[name="Executor"]', 5000)
            .click('select[name="Executor"]')
            .pause(2000)
            .keys(client.Keys.ARROW_DOWN+client.Keys.RETURN)
    },
    'Проверка группировки и чекбоксов': function (client) {
        client
            .useXpath().waitForElementVisible("//div[contains(@class,'panel-group')]//a", 5000)
            .click("//div[contains(@class,'panel-group')]//a")
            .pause(2500)
            .click("//div[contains(@class,'panel-group')]//a")
            //Чекбокс подгруппы
            .useCss().click('ins[data-original-title="Выбрать все/Снять выделение"]')
            .pause(2500)
            .click('ins[data-original-title="Выбрать все/Снять выделение"]')
            .pause(2500)
    },
    'Отправка исследования в МедЛаб': function (client) {
        client
            .useCss().click('ins[data-original-title=""]')
            .pause(1500)
            .click('button[class="btn btn-sm btn-primary pull-right"]')
            .useXpath().waitForElementVisible("//p[text()='Вы действительно хотите отправить исследования в лабораторию МедЛаб']", 500)
            .useCss().assert.visible('button[class="cancel"]')
            .pause(2000)
            .click('button[class="confirm"]')
            .pause(3000)
            .end();
        client.perform(function (client) {
            console.log('Успешное завершение теста Медсестры сортировочного кабинета');
            client()
        })
    }
};
