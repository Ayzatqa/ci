/**
 * Created by Mr. Ayzat on 24.10.2016.
 */
module.exports = {
    'Бухгалтер': function (client) {
        client
            .windowMaximize()
            .url('http://rc.nexmed.cargorun.ru/')
    },
    'Авторизация бухгалтера': function (client) {
        client
            .waitForElementPresent('input[name=Login]', 500)
            .setValue('input[name=Login]', "vasil`eva.yuliya")
            .setValue('input[name=Password]', 'qwerty')
            .useXpath().click("//button[text()='Войти']")
            .pause(5000)
            .end();
        client.perform(function (client) {
            console.log('Успешное завершение теста Бухгалтера');
            client()
        })
    }
};