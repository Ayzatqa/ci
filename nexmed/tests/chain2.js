/**
 * Created by Mr. Ayzat on 07.10.2016.
 */
module.exports = {
    tags:['Nexmed, procedurnaya medsestra'],
    'Процедурная медсестра': function (client) {
        client
            .windowMaximize()
            .url('http://rc.nexmed.ru/')
    },
    'Авторизация процедурной медсестры': function (client) {
        client
            .waitForElementVisible('input[name=Login]', 5000)
            .setValue('input[name=Login]', 'salixova.rajlya')
            .setValue('input[name=Password]', 'qwerty')
            .useXpath().click("//button[text()='Войти']")
    },
    'Входящие': function (client) {
        client
            .useXpath().waitForElementVisible("//span[text()='Входящие']", 5000)
            .assert.visible("//h4[text()='АРМ Процедурная медсестра']")
            .click("//span[text()='Входящие']")
            .pause(2000)
    },
    'Карточка назначений пациента': function (client) {
        client
            .useXpath().waitForElementVisible("//td[contains(@class,'group')]//a", 5000)
            .click("//td[contains(@class,'group')]//a")
            .pause(2000)
    },
    'Проверка выделения по подруппам б/м': function (client) {
        client
            .useCss().click('ins[data-original-title="Выделить подгруппу по биоматериалу"]')
            .pause(2000)
            .click('button[class="btn btn-primary btn-sm pull-right"]')
            .pause(4000)
            .useXpath().assert.visible("//label[text()='Маркировка б/м']")
            .click("//button[text()='Отмена']")
            .pause(2000)
            .useCss().click('ins[data-original-title="Выделить подгруппу по биоматериалу"]')
            .pause(2000)
    },
    'Проверка выделения всех назначений': function (client) {
        client
            .useCss().waitForElementVisible('th[class="sorting"]', 5000)
            .click('th[class="sorting"]')
            .click('ins[data-original-title="Выделить все"]')
            .waitForElementVisible('button[class="btn btn-primary btn-sm pull-right"]', 5000)
            .pause(2000)
            .click('button[class="btn btn-primary btn-sm pull-right"]')
            .pause(2000)
            .useXpath().click("//button[text()='Отмена']")
            .pause(2000)
            .click("//span[text()='Входящие']")
    },
    'Проверка фильтрации и "Срочно"': function(client) {
        client
            //Чекбокс CITO
            .useCss().waitForElementPresent('ins[class="iCheck-helper"]', 5000)
            .pause(3000)
            .click('ins[class="iCheck-helper"]')
            .setValue('input[class="form-control table-search"]', 'Уинстон')
            //Кнопка Фильтрация
            .click('button[class="btn btn-primary2"]')
            .pause(2000)
            // .useXpath().waitForElementPresent("//td[text()='В таблице отсутствуют данные']", 3000)
            // .useCss()
            .click('input[name=Date]')
            .useXpath().waitForElementVisible("//td[text()='19']", 3000)
            .click("//td[text()='19']")
            .waitForElementPresent("//span[text()='Входящие']", 5000)
            .click("//span[text()='Входящие']")
            .pause(2000);
            // .waitForElementPresent("//td[text()='В таблице отсутствуют данные']", 3000)
    },
    'Проверка группировки в списке ': function (client) {
        client
            .useXpath().waitForElementVisible("//td[contains(@class,'group')]", 5000)
            .click("//td[contains(@class,'group')]")
            .useCss().waitForElementVisible('i[class="fa fa-close"]', 5000)
            .assert.visible('i[class="fa fa-close"]')
    },
    'Проверка маркировки': function (client) {
        client
            .useXpath().waitForElementVisible("//td[contains(@class,'group')]//a", 5000)
            .click("//td[contains(@class,'group')]//a")
            .pause(2000)
    },
    'Нажатие на "Маркировка" не выбрав назначение': function(client) {
        client
            .useCss().click('button[class="btn btn-primary btn-sm pull-right"]')
            .pause(2000)
            .useXpath().assert.visible("//span[text()='Вам необходимо выбрать одно или несколько исследований, прежде чем указать маркировку.']")
            .click("//button[text()='Отмена']")
            .pause(3000)
    },
    'Нажатие на "Маркировка" выбрав назначение': function(client) {
        client
            .useXpath().waitForElementVisible("//thead/tr/th[1]/div", 5000)
            .click("//thead/tr/th[1]/div")
            .pause(2000)
            .useCss().click('button[class="btn btn-primary btn-sm pull-right"]')
            .useXpath().waitForElementVisible("//h4[text()='Выбранные исследования']", 5000)
            // .assert.visible("//label[text()='Маркировка б/м']")
            // .assert.visible("//button[text()='Отмена']")
            // .assert.visible("//h4[text()='Указание маркировки']")
            // .assert.visible("//li[text()='Общий анализ крови']")
    },
    'Указание маркировки': function (client) {
        client
            .useCss().setValue('input[name=Marking]', 'Тестирование')
            .click('button[class="btn btn-primary"]')
            .pause(3000)
            .end();
        client.perform(function (client) {
            console.log('Успешное завершение теста Процедурной медсестры');
            client()
        })
    }
};