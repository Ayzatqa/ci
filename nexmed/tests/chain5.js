/**
 * Created by Mr. Ayzat on 07.10.2016.
 */
module.exports = {
    'Лаборант': function (client) {
        client
            .windowMaximize()
            .url('http://rc.nexmed.cargorun.ru/')
    },
    'Авторизация лаборанта': function (client) {
        client
            .waitForElementPresent('input[name=Login]', 500)
            .setValue('input[name=Login]', "kady'rov.danis")
            .setValue('input[name=Password]', 'qwerty')
            .useXpath().click("//button[text()='Войти']")
            .pause(5000)
            .end();
        client.perform(function (client) {
            console.log('Успешное завершение теста Лаборанта');
            client()
        })
    }
};