/**
 * Created by Mr. Ayzat on 31.10.2016.
 */
/**
 * Created by Mr. Ayzat on 07.10.2016.
 */
module.exports = {
    tags:['Nexmed, doctor'],
    'Врач': function (client) {
        client
            .windowMaximize()
            .url('http://rc.nexmed.ru/')
    },
    'Авторизация врача': function (client) {
        client
            .waitForElementVisible('input[name=Login]', 5000)
            .setValue('input[name=Login]', 'gajsin.rishat')
            .setValue('input[name=Password]', 'qwerty')
            .useXpath().click("//button[text()='Войти']")
    },
    'Список пациентов': function (client) {
        client
            .useXpath().waitForElementVisible("//span[text()='Пациенты']", 5000)
            .assert.visible("//h4[text()='АРМ Врач']")
            .click("//span[text()='Пациенты']")
        // },
        // 'Поиск пациента': function (client) {
        //     client
        //         .useCss().waitForElementVisible('input[id=SearchPatients]', 5000)
        // },
        // 'Поиск пациента по ФИО': function (client) {
        //     client
        //         .useCss().setValue('input[id=SearchPatients]', 'Кашапов')
        //         .pause(2000)
        //         .keys(client.Keys.ARROW_DOWN+client.Keys.RETURN)
        //         .useXpath().waitForElementVisible("//small[text()='Кашапов Роберт Дмитриевич']", 5000)
        //         .click("//span[text()='Пациенты']")
        //         .useCss().waitForElementVisible('input[id=SearchPatients]', 5000)
        // },
        // 'Поиск пациента по СНИЛС': function (client) {
        //     client
        //         .useCss().setValue('input[id=SearchPatients]', '152-125-1')
        //         .pause(2000)
        //         .keys(client.Keys.ARROW_DOWN+client.Keys.RETURN)
        //         .useXpath().waitForElementVisible("//small[text()='Кашапов Роберт Дмитриевич']", 5000)
        //         .click("//span[text()='Пациенты']")
        //         .pause(2000);
        // },
        // 'Поиск пациента по номеру телефона': function (client) {
        //     client
        //         .useCss().setValue('input[id=SearchPatients]', '+7999')
        //         .pause(2000)
        //         .keys(client.Keys.ARROW_DOWN + client.Keys.RETURN)
        //         .useXpath().waitForElementVisible("//small[text()='Кашапов Роберт Дмитриевич']", 5000)
        //         .click("//span[text()='Пациенты']")
        //         .pause(2000);
    },
    'Добавление пациента': function (client) {
        client
            .useXpath().click("//a[text()='Добавить пациента']")
            .pause(2000)
    },
    'Заполнение общей информации': function (client) {
        client
            .useCss().setValue('input[name=SecondName]', 'Кашапов')
            .setValue('input[name=FirstName]', 'Роберт')
            .setValue('input[name=Patronymic]', 'Дмитриевич');
        client.setValue('input[name=Birthday]', ['20.11.1996', client.Keys.RETURN])
            .click('input[value=Female]')
            .setValue('input[name=Address]', 'г. Набережные Челны, дом 11, кв 5.')
    },
    'Заполнение личных данных': function (client) {
        client
            .useCss().setValue('input[name=Snils]', '152-125-124-12-29')
            .setValue('input[name=Polis]', '№3234364632')
            .setValue('input[name=PolisData]', 'Поликлиника №3')
            .setValue('input[name=Passport]', '9108 8522234')
            .setValue('input[name=PassportData]', 'ОУФМС по РФ, РБ, ленниа 25')
    },
    'Заполнение контактной информации': function (client) {
        client
            .setValue('input[name=PhoneNumber]', '+799933330011')
            .setValue('input[name=Email]', 'elvar@gmail.com')
    },
    'Добавить': function (client) {
        client
            .useXpath().click("//button[text()='Добавить']")
            .pause(4000);
        // },
        // 'Редактирование информации пациента': function (client) {
        //     client
        //         .useCss().waitForElementVisible('i[class="fa fa-pencil"]', 5000)
        //         .click('i[class="fa fa-pencil"]')
        //         .pause(4000)
        // },
        // 'Редактирование общей информации': function (client) {
        //     client
        //         .useCss().clearValue('input[name=SecondName]')
        //         .setValue('input[name=SecondName]', 'Сычев')
        //         .clearValue('input[name=FirstName]')
        //         .setValue('input[name=FirstName]', 'Александр')
        //         .clearValue('input[name=Patronymic]')
        //         .setValue('input[name=Patronymic]', 'Петрович')
        //         .clearValue('input[name=Birthday]');
        //     client.setValue('input[name=Birthday]', ['19.11.1997', client.Keys.RETURN])
        //         .click('input[value=Male]')
        //         .clearValue('input[name=Address]')
        //         .setValue('input[name=Address]', 'г. Казань, дом 11, кв 2.')
        // },
        // 'Редактирование личных данных': function (client) {
        //     client
        //         .clearValue('input[name=Snils]')
        //         .setValue('input[name=Snils]', '153-115-224-32-14')
        //         .clearValue('input[name=Polis]')
        //         .setValue('input[name=Polis]', '№3734869685')
        //         .clearValue('input[name=PolisData]')
        //         .setValue('input[name=PolisData]', 'Поликлиника №5')
        //         .clearValue('input[name=Passport]')
        //         .setValue('input[name=Passport]', '8909 832123')
        //         .clearValue('input[name=PassportData]')
        //         .setValue('input[name=PassportData]', 'ОУФМС по РФ, Москва, Леннина 11')
        // },
        // 'Редактирование контактной информации': function (client) {
        //     client
        //         .clearValue('input[name=PhoneNumber]')
        //         .setValue('input[name=PhoneNumber]', '+79991113331')
        //         .clearValue('input[name=Email]')
        //         .setValue('input[name=Email]', 'sychev111@gmail.com')
        // },
        // 'Сохранение': function (client) {
        //     client
        //         .useXpath().click("//button[text()='Сохранить изменения']")
        //         .useCss().waitForElementVisible('a[class="btn btn-primary btn-sm btn-xs pull-right"]', 5000)
        //         .useXpath().assert.visible("//small[text()='Сычев Александр Петрович']")
        //         .click("//span[text()='Пациенты']")
        //         .pause(4000);
        // },
        // 'Удаление пациента': function (client) {
        //     client
        //         //Кнопка удалить пациента
        //         .useCss().waitForElementVisible('button[class="btn btn-danger btn-xs"]', 5000)
        //         .click('button[class="btn btn-danger btn-xs"]')
        //         .useXpath().waitForElementVisible("//td[text()='Пол']", 5000)
        //         .assert.visible("//button[text()='Отмена']")
        //         .useCss().assert.visible('button[class="btn btn-danger"]')
        //         .click('button[class="btn btn-danger"]')
        //         .pause(3000)
        //         .refresh()
        //         .pause(4000)
    },
    'Пациент': function (client) {
        client
            .useXpath().waitForElementVisible("//a[text()='Кашапов Роберт Дмитриевич']", 5000)
            .click("//a[text()='Кашапов Роберт Дмитриевич']")
            .pause(3000)
    },
    'Карточка пациента': function (client) {
        client
        //Кнопка Добавить исследование
            .useCss().waitForElementPresent('a[class="btn btn-primary btn-sm btn-xs pull-right"]', 8000)
            .click('a[class="btn btn-primary btn-sm btn-xs pull-right"]')
            .pause(3000)
    },
    'Добавление исследования': function (client) {
        client
            .useXpath().waitForElementVisible("//label[text()='Исследование']", 5000)
            .assert.visible("//label[text()='CITO']")
        //Поле Исследование
            .useCss().click('span[class="select2-selection select2-selection--single"]')
            .pause(2000)
            //Поле Поиска Исследования
            .setValue('input[type=search]', 'Мочевина')
            .pause(1500)
            .keys(client.Keys.ENTER)
            .click('ins[class=iCheck-helper]')
            .useXpath().click("//button[text()='Назначить']")
            .pause(2000)
            .useCss().waitForElementVisible('a[class="btn btn-primary btn-sm btn-xs pull-right"]', 5000)
            .click('a[class="btn btn-primary btn-sm btn-xs pull-right"]')
            .useXpath().waitForElementVisible("//label[text()='Исследование']", 5000)
            .assert.visible("//label[text()='CITO']")
            .useCss().click('span[class="select2-selection select2-selection--single"]')
            .pause(2000)
            .setValue('input[type=search]', 'Аланинаминотрансфераза')
            .pause(1500)
            .keys(client.Keys.ENTER)
            .click('ins[class=iCheck-helper]')
            .useXpath().click("//button[text()='Назначить']")
            .pause(2000)
            .useCss().waitForElementVisible('a[class="btn btn-primary btn-sm btn-xs pull-right"]', 5000)
            .click('a[class="btn btn-primary btn-sm btn-xs pull-right"]')
            .useXpath().waitForElementVisible("//label[text()='Исследование']", 5000)
            .assert.visible("//label[text()='CITO']")
            .useCss().click('span[class="select2-selection select2-selection--single"]')
            .pause(2000)
            .setValue('input[type=search]', 'Общий анализ крови')
            .pause(1500)
            .keys(client.Keys.ENTER)
            .click('ins[class=iCheck-helper]')
            .useXpath().click("//button[text()='Назначить']")
    },
    'Отклонение исследования': function (client) {
        client
            .pause(2000)
            .useCss().click('th[class="sorting_asc"]')
            .pause(2500)
            .keys(client.Keys.ARROW_DOWN)
            .keys(client.Keys.ARROW_DOWN)
            .keys(client.Keys.ARROW_DOWN)

            .pause(4000);
        //Кнопка Отменить
        client.saveScreenshot('/nexmed/screens/vrach/otmenanazn.png')
            .useCss().waitForElementVisible('button[data-title="Отмена назначения"]', 5000)
            .click('button[data-title="Отмена назначения"]')
            .pause(3000)
            //Кнопка Нет
            .assert.visible('button[class="cancel"]')
        //Кнопка Да
            .assert.visible('button[class="confirm"]');
        client.saveScreenshot('/nexmed/screens/vrach/knopkaDa.png')
            .click('button[class="confirm"]')
            .pause(3000)
            .end();
        client.perform(function (client) {
            console.log('Успешное завершение теста Врача');
            client()
        })
    }
};